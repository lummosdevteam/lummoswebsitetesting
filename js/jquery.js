

$(document).ready(function(){
    
        var button = $(".button");
        
        button.mouseenter(function(){
                $(this).stop().animate({ backgroundColor: "white" , color: "black" , queue: false }, "fast");
            });
        
        button.mouseleave(function(){
                $(this).stop().animate({ backgroundColor: "transparent" , color: "white" , queue: false }, "fast");
            });
        
        var contact = $(".contact");
        
        contact.mouseenter(function(){
                contact.stop().animate({ backgroundColor: "white" , color: "black" , width: 250 , height: 100 , queue: false});
                contact.css({ width: 200 , height: 100, textAlign: "center" });
                $(".contact p").slideUp(200).html(" <b> Email: exampleemail@email.com <br> Number: (+123) 1234-123 <br> Number: (+234) 2345-234 </b> ").slideDown(200);
            });
        
        contact.mouseleave(function(){
                contact.stop().animate({ backgroundColor: "transparent" , color: "white" , width: 100, height: 40 , queue: false });
                contact.css({ textAlign: "center" });
                $(".contact p").slideUp(100).html("Contacts").slideDown(100);
            });
        
        var title = $(".title");


        title.text(" WELCOME ").delay(2000).slideUp(600);
        setTimeout(function(){
                        title.html(" COMPANY ").slideDown(600);
                }, 2600);
    });
